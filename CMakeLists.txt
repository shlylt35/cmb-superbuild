cmake_minimum_required(VERSION 3.11.2)

project(cmb-superbuild)

list(INSERT CMAKE_MODULE_PATH 0
  "${CMAKE_CURRENT_LIST_DIR}/cmake")

macro (superbuild_setup_variables)
  include(SuperbuildVersionMacros)

  if (cmb_SOURCE_SELECTION STREQUAL "git")
    # Assuming master; just use the latest version, but let the user set their
    # own version in case it is actually a branch from some other version.
    set(CMB_VERSION_DEFAULT "<FETCH_FROM_GIT>"
      CACHE STRING "The default version of CMB to use if it cannot be detected")
    if (CMB_VERSION_DEFAULT STREQUAL "<FETCH_FROM_GIT>")
      include(cmbVersion)
      set(CMB_VERSION_DEFAULT "${cmb_version_major}.${cmb_version_minor}.${cmb_version_patch}")
    endif ()
    mark_as_advanced(CMB_VERSION_DEFAULT)
    set(cmb_default_version "${CMB_VERSION_DEFAULT}")
  elseif (cmb_SOURCE_SELECTION STREQUAL "source")
    # If it is a git repo, we'll get it that way. Otherwise, we will look at the
    # `version.txt` in the checkout.
    set(cmb_default_version "")
  else ()
    # The selection is the version number; use it.
    set(cmb_default_version "${cmb_SOURCE_SELECTION}")
  endif()
  superbuild_set_version_variables(cmb "${cmb_default_version}" "cmb-version.cmake" "version.txt")
  set(cmb_version "${cmb_version_major}.${cmb_version_minor}")

  if (smtk_SOURCE_SELECTION STREQUAL "git")
    # Assuming master; just use the latest version, but let the user set their
    # own version in case it is actually a branch from some other version.
    set(SMTK_VERSION_DEFAULT "<FETCH_FROM_GIT>"
      CACHE STRING "The default version of SMTK to use if it cannot be detected")
    if (SMTK_VERSION_DEFAULT STREQUAL "<FETCH_FROM_GIT>")
      include(smtkVersion)
      set(SMTK_VERSION_DEFAULT "${smtk_version_major}.${smtk_version_minor}.${smtk_version_patch}")
    endif ()
    mark_as_advanced(SMTK_VERSION_DEFAULT)
    set(smtk_default_version "${SMTK_VERSION_DEFAULT}")
  elseif (smtk_SOURCE_SELECTION STREQUAL "source")
    # If it is a git repo, we'll get it that way. Otherwise, we will look at the
    # `version.txt` in the checkout.
    set(smtk_default_version "")
  else ()
    # The selection is the version number; use it.
    set(smtk_default_version "${smtk_SOURCE_SELECTION}")
  endif()
  superbuild_set_version_variables(smtk "${smtk_default_version}" "smtk-version.cmake" "version.txt")
  set(smtk_version "${smtk_version_major}.${smtk_version_minor}")

  superbuild_set_version_variables(paraview "5.6.0" "paraview-version.cmake" "version.txt")
  set(paraview_version "${paraview_version_major}.${paraview_version_minor}")
  superbuild_set_version_variables(vtk "8.1.0" "vtk-version.cmake")
  set(vtk_version "${vtk_version_major}.${vtk_version_minor}")
endmacro ()

function (superbuild_find_projects var)
  include(CMBSuperbuildMacros)

  set(projects
    boost
    bzip2
    cmb
    cmbusersguide
    cmbworkflows
    cumulus
    cxx11
    eigen
    fortran
    freetype
    ftgl
    gdal
    hdf5
    lapack
    matplotlib
    moab
    netcdf
    nlohmannjson
    numpy
    opencv
    paraview
    paraviewweb
    paraviewwebvisualizer
    paraviewweblightviz
    png
    pyarc
    pybind11
    python
    pythondiskcache
    pythongirderclient
    pythonrequests
    pythonrequeststoolbelt
    pythonsetuptools
    pythonsix
    qt
    qt4
    qt5
    remus
    smtk
    smtkusersguide
    szip
    vtk
    vtkonly
    vxl
    zeromq
    zlib)

  if (NOT WIN32)
    list(APPEND projects
      fontconfig
      gperf
      libxml2)
  endif ()

  if (WIN32)
    list(APPEND projects
      ftjam
      pythonsetuptools
      pywin32)
  endif ()

  if (NOT DEVELOPER_MODE_smtk)
    list(APPEND projects
      smtkprojectmanager
      smtkresourcemanagerstate)
  endif ()
  # For now it's a convenient option to control smtk documentation
  option(ENABLE_DOCUMENTATION "Enable smtk documentation" ON)

  option(USE_NONFREE_COMPONENTS "Enable the non-free parts of the superbuild" OFF)
  mark_as_advanced(USE_NONFREE_COMPONENTS)
  if (USE_NONFREE_COMPONENTS)
    list(APPEND projects
      triangle)
  endif ()

  set("${var}"
    ${projects}
    PARENT_SCOPE)
endfunction ()

function (superbuild_sanity_check)
  if (vtkonly_enabled AND paraview_enabled)
    message(FATAL_ERROR
      "Only one of the following projects should be enabled: vtkonly, paraview.")
  endif ()
  if (NOT smtk_enabled AND NOT cmb_enabled)
    message(FATAL_ERROR
      "At least one of the following projects must be enabled: cmb, smtk.")
  endif ()
endfunction ()

function (superbuild_add_packaging)
  if (NOT cmb_enabled OR DEVELOPER_MODE_cmb)
    return ()
  endif ()

  if (WIN32)
    set(generators
      ZIP)
  elseif (APPLE)
    set(generators
      DragNDrop)
  else ()
    set(generators
      TGZ)
  endif ()

  if (qt5_built_by_superbuild)
    set(Qt5_DIR "${superbuild_install_location}/lib/cmake/Qt5")
  endif ()
  if (USE_SYSTEM_qt5)
    list(APPEND superbuild_export_variables
      Qt5_DIR)
    find_package(Qt5 QUIET REQUIRED COMPONENTS Core)
    set(qt5_version "${Qt5Core_VERSION_MAJOR}.${Qt5Core_VERSION_MINOR}")
  else ()
    set(qt5_version "${qt5_SOURCE_SELECTION}")
  endif ()
  list(APPEND superbuild_export_variables
    qt5_version)

  include(cmb.suffix)
  list(APPEND superbuild_export_variables
    CMB_PACKAGE_SUFFIX)

  if (cmb_enabled)
    set(CMB_PACKAGE_FILE_NAME ""
      CACHE STRING "If set, overrides the generated filename for the package")
    list(APPEND superbuild_export_variables
      CMB_PACKAGE_FILE_NAME)
  endif ()

  foreach (generator IN LISTS generators)
    superbuild_add_extra_package_test(modelbuilder "${generator}"
      LABELS  "CMB"
      TIMEOUT 6400)
  endforeach ()
endfunction ()

macro (superbuild_setup_tests)
  if (cmb_enabled)
    set(DROP_LOCATION "/submit.php?project=CMB")
  elseif (smtk_enabled)
    set(DROP_LOCATION "/submit.php?project=SMTK")
  endif ()
endmacro ()

function (superbuild_add_tests)
  set_property(GLOBAL APPEND
    PROPERTY
      superbuild_test_projects cmb smtk)
endfunction ()

set(CMB_TEST_DATA_ROOT "" CACHE PATH "Location of CMB's testing data")

# Default Qt5 to ON.
set(_superbuild_default_qt5 ON)

set(qt5_skip_modules
  qtconnectivity
  qtgamepad
  qtlocation
  qtmultimedia
  qtsensors
  qtserialport
  qtwayland
  qtwebchannel
  qtwebengine
  qtwebsockets)

set(BOOST_ADDITIONAL_LIBRARIES "" CACHE PATH "Additional boost libraries to build")
mark_as_advanced(BOOST_ADDITIONAL_LIBRARIES)

set(boost_minimum_version 1.64.0)
set(boost_libraries
  atomic
  chrono
  date_time
  filesystem
  log
  regex
  serialization
  system
  thread
  timer
  ${BOOST_ADDITIONAL_LIBRARIES})

configure_file(
  "${CMAKE_CURRENT_LIST_DIR}/cmake/CTestCustom.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/CTestCustom-cmb.cmake"
  @ONLY)

if (__BUILDBOT_INSTALL_LOCATION)
  set(superbuild_install_location "${__BUILDBOT_INSTALL_LOCATION}")
endif ()

set(suppress_projects
  boost
  freetype
  ftgl
  gdal
  hdf5
  netcdf
  numpy
  png
  pybind11
  szip
  vxl
  zeromq
  zlib)
foreach (suppress_project IN LISTS suppress_projects)
  set("_superbuild_suppress_${suppress_project}_output" ON)
endforeach ()

list(APPEND superbuild_version_files
  "${CMAKE_CURRENT_LIST_DIR}/versions.cmake")
list(APPEND superbuild_ctest_custom_files
  "${CMAKE_CURRENT_BINARY_DIR}/CTestCustom-cmb.cmake")
list(APPEND superbuild_project_roots
  "${CMAKE_CURRENT_LIST_DIR}/projects")
add_subdirectory(superbuild)
