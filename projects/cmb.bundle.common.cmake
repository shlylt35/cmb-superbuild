# Consolidates platform independent stub for cmb.bundle.cmake files.

include(cmb-version)
include(paraview-version)

set(CPACK_PACKAGE_VENDOR "Kitware, Inc.")
set(CPACK_PACKAGE_VERSION_MAJOR ${cmb_version_major})
set(CPACK_PACKAGE_VERSION_MINOR ${cmb_version_minor})
set(CPACK_PACKAGE_VERSION_PATCH ${cmb_version_patch}${cmb_version_suffix})
if (CMB_PACKAGE_SUFFIX)
  set(CPACK_PACKAGE_VERSION_PATCH ${CPACK_PACKAGE_VERSION_PATCH}-${CMB_PACKAGE_SUFFIX})
endif ()

if (NOT DEFINED package_filename)
  set(package_filename "${CMB_PACKAGE_FILE_NAME}")
endif ()

if (package_filename)
  set(CPACK_PACKAGE_FILE_NAME "${package_filename}")
else ()
  set(CPACK_PACKAGE_FILE_NAME
    "${cmb_package_name}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
endif ()

# Set the license file.
set(CPACK_RESOURCE_FILE_LICENSE
    "${superbuild_source_directory}/License.txt")

list(SORT cmb_programs_to_install)
list(REMOVE_DUPLICATES cmb_programs_to_install)

function (cmb_add_plugin output)
  set(contents "<?xml version=\"1.0\"?>\n<Plugins>\n</Plugins>\n")
  foreach (name IN LISTS ARGN)
    set(plugin_directive "  <Plugin name=\"${name}\" auto_load=\"0\" />\n")
    string(REPLACE "</Plugins>" "${plugin_directive}</Plugins>" contents "${contents}")
  endforeach ()
  file(WRITE "${output}" "${contents}")
endfunction ()

set(paraview_executables)
if (cmb_install_paraview_server)
  list(APPEND paraview_executables
    pvserver
    pvdataserver
    pvrenderserver)
endif ()
if (cmb_install_paraview_python)
  list(APPEND paraview_executables
    pvbatch
    pvpython)
endif ()

# Currently, ModelBuilder is predominantly ParaView + a suite of plugins from
# SMTK. As the latest iteration of ModelBuilder developed, we encountered
# inconsistencies during development & packaging with the automatic inclusion of
# SMTK's plugins. As a workaround, we created a library in SMTK named
# "smtkDefaultPlugins" that links against the plugins SMTK has been configured
# to build and exposes methods for programatically setting them up in
# ModelBuilder.
#
# When ModelBuilder links against smtkDefaultPlugins, the packager treats
# smtkDefaultPlugins' dependent libraries (the plugins) as libraries and
# puts them with the rest of the libraries. The code below is a hand-maintained
# whitelist of SMTK's plugins; the packager treats these as plugins and puts
# them in a plugins directory. When the packager comes across a plugin that is
# also linked as a library, it gets confused and installs it into both the
# library and plugin directories, but only fixes the runtime paths for the
# copy in the plugin directory.
#
# SMTK's ability to create a library that links its plugins into a consuming
# application is currently less fault-prone than ParaView's default plugin
# system. Future updates to ParaView may obviate the need for this ability,
# however. Until then, we disable the following logic for SMTK's plugins.
set(LINK_TO_PLUGINS True)
if (NOT ${LINK_TO_PLUGINS})
  set(cmb_plugins_smtk
    smtkAttributePlugin
    smtkDelaunayPlugin
    smtkDiscreteSessionPlugin
    smtkMeshPlugin
    smtkMeshSessionPlugin
    smtkModelPlugin
    smtkOpenCVPlugin
    smtkOperationPlugin
    smtkPQComponentsPlugin
    smtkPQOperationViewsPlugin
    smtkPVServerExtPlugin
    smtkPolygonSessionPlugin
    smtkRemusMeshOperationPlugin
    smtkVTKOperationsPlugin
  )

  if (matplotlib_enabled)
    list(APPEND cmb_plugins_smtk
      smtkMatplotlibPlugin)
  endif ()

  if (vxl_enabled)
    list(APPEND cmb_plugins_smtk
      smtkVXLOperationViewsPlugin)
  endif ()
endif()

set(cmb_plugins_cmb
  cmbPostProcessingModePlugin
)

set(cmb_plugins_standalone)
if (smtkprojectmanager_enabled)
  list(APPEND cmb_plugins_standalone smtkProjectManagerPlugin)
endif()
if (smtkresourcemanagerstate_enabled)
  list(APPEND cmb_plugins_standalone smtkReadWriteResourceManagerStatePlugin)
endif()

if (cumulus_enabled)
  set(cmb_plugins_paraview
    SLACTools
  )
endif()

set(cmb_python_modules
  smtk
  paraview
  cinema_python
  pygments
  six
  vtk
  vtkmodules)

if (matplotlib_enabled)
  list(APPEND cmb_python_modules
    matplotlib)
endif ()

if (numpy_enabled)
  list(APPEND cmb_python_modules
    numpy)
endif ()

if (pythongirderclient_enabled)
  list(APPEND cmb_python_modules
    diskcache
    requests
    requests_toolbelt
    girder_client)
endif ()

if (paraviewweb_enabled)
  list(APPEND cmb_python_modules
    autobahn
    constantly
    incremental
    twisted
    wslink
    zope)

  if (WIN32)
    list(APPEND cmb_python_modules
      adodbapi
      isapi
      pythoncom
      win32com)
  endif ()
endif ()

if (qt5_enabled)
  include(qt5.functions)

  set(qt5_plugin_prefix)
  if (NOT WIN32)
    set(qt5_plugin_prefix "lib")
  endif ()

  set(qt5_plugins
    sqldrivers/${qt5_plugin_prefix}qsqlite)

  if (WIN32)
    list(APPEND qt5_plugins
      platforms/qwindows)

    if (NOT qt5_version VERSION_LESS "5.10")
      list(APPEND qt5_plugins
        styles/libqwindowsvistastyle)
    endif ()
  elseif (APPLE)
    list(APPEND qt5_plugins
      platforms/libqcocoa
      printsupport/libcocoaprintersupport)

    if (NOT qt5_version VERSION_LESS "5.10")
      list(APPEND qt5_plugins
        styles/libqmacstyle)
    endif ()
  elseif (UNIX)
    list(APPEND qt5_plugins
      platforms/libqxcb
      platforminputcontexts/libcomposeplatforminputcontextplugin
      xcbglintegrations/libqxcb-glx-integration)
  endif ()

  superbuild_install_qt5_plugin_paths(qt5_plugin_paths ${qt5_plugins})
else ()
  set(qt5_plugin_paths)
endif ()

function (cmb_install_pdf project filename)
  if (${project}_enabled)
    install(
      FILES       "${superbuild_install_location}/doc/${filename}"
      DESTINATION "${cmb_doc_dir}"
      COMPONENT   superbuild)
  endif ()
endfunction ()

function (cmb_install_extra_data)
  if (cmb_doc_dir)
    cmb_install_pdf(cmbusersguide "CMBUsersGuide.pdf")
    cmb_install_pdf(smtkusersguide "SMTKUsersGuide.pdf")
  endif ()
endfunction ()
