set(extra_cmake_args)
if (UNIX AND NOT APPLE)
  list(APPEND extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

set(response_file)
if (WIN32)
  # Force response file usage. The command line gets way too long on Windows
  # without this. Once VTK_USE_FILE and PARAVIEW_USE_FILE are gone, this can be
  # removed again.
  set(response_file -DCMAKE_NINJA_FORCE_RESPONSE_FILE:BOOL=ON)
endif ()

superbuild_add_project(smtkprojectmanager
  DEBUGGABLE
  DEFAULT_ON
  DEPENDS boost cxx11 paraview qt qt5 smtk
  CMAKE_ARGS
    ${extra_cmake_args}
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    ${response_file})
