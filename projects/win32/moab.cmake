include("${CMAKE_CURRENT_LIST_DIR}/../moab.cmake")

superbuild_apply_patch(moab quote-compiler-path
  "Add quotations around compiler path to avoid whitespace warnings.")

superbuild_apply_patch(moab remove-flags-from-link-list
  "Remove CMake error where linker flags are a part of the link library list")

superbuild_apply_patch(moab msvc-fixes
  "Many windows-specific fixes for moab (not developed on windows).")
