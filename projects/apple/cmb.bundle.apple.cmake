foreach(program IN LISTS cmb_programs_to_install)
  set(plugins)
  set(plugin_paths)

  if (program STREQUAL "modelbuilder")
    # Install CMB plugins in modelbuilder app
    foreach(plugin IN LISTS cmb_plugins_cmb)
      list(APPEND plugin_paths "${superbuild_install_location}/Applications/modelbuilder.app/Contents/Plugins/lib${plugin}.dylib")
    endforeach()
    list(APPEND plugins ${cmb_plugins_cmb})

    # Install CMB plugins in modelbuilder app
    foreach(plugin IN LISTS cmb_plugins_standalone)
      find_library(plugin_path "lib${plugin}.dylib" ${superbuild_install_location}/lib)
      list(APPEND plugin_paths ${plugin_path})
    endforeach()
    list(APPEND plugins ${cmb_plugins_standalone})

    # Install paraview plugins in modelbuilder app
    foreach (plugin IN LISTS cmb_plugins_paraview)
      list(APPEND plugin_paths "${superbuild_install_location}/Applications/paraview.app/Contents/Plugins/lib${plugin}.dylib")
    endforeach()
    list(APPEND plugins ${cmb_plugins_paraview})
  endif()

  superbuild_apple_create_app(
    "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
    "${program}.app"
    "${superbuild_install_location}/Applications/${program}.app/Contents/MacOS/${program}"
    CLEAN
    PLUGINS ${plugin_paths}
    SEARCH_DIRECTORIES
            "${superbuild_install_location}/lib")
  foreach (icon_filename MacIcon.icns pvIcon.icns)
    set(icon_path "${superbuild_install_location}/Applications/${program}.app/Contents/Resources/${icon_filename}")
    if (EXISTS "${icon_path}")
      install(
        FILES       "${icon_path}"
        DESTINATION "${cmb_package}/${program}.app/Contents/Resources"
        COMPONENT   superbuild)
    endif ()
  endforeach ()
  install(
    FILES       "${superbuild_install_location}/Applications/${program}.app/Contents/Info.plist"
    DESTINATION "${cmb_package}/${program}.app/Contents"
    COMPONENT   superbuild)

  foreach (executable IN LISTS paraview_executables)
    superbuild_apple_install_utility(
      "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
      "${program}.app"
      "${superbuild_install_location}/Applications/paraview.app/Contents/bin/${executable}"
      SEARCH_DIRECTORIES
              "${superbuild_install_location}/lib")
  endforeach ()

  install(CODE
    "file(MAKE_DIRECTORY \"\${CMAKE_INSTALL_PREFIX}/${cmb_package}/${program}.app/Contents/Resources\")
    file(WRITE \"\${CMAKE_INSTALL_PREFIX}/${cmb_package}/${program}.app/Contents/Resources/qt.conf\" \"\")"
    COMPONENT superbuild)

  set(plugins_file "${CMAKE_CURRENT_BINARY_DIR}/${program}.plugins")
  cmb_add_plugin("${plugins_file}" ${plugins})

  install(
    FILES       "${plugins_file}"
    DESTINATION "${cmb_package}/${program}.app/Contents/Plugins"
    COMPONENT   superbuild
    RENAME      ".plugins")

  superbuild_apple_install_python(
    "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
    "${program}.app"
    MODULES ${cmb_python_modules}
    MODULE_DIRECTORIES
            "${superbuild_install_location}/lib/python2.7/site-packages"
            "${superbuild_install_location}/Applications/paraview.app/Contents/Python"
    SEARCH_DIRECTORIES
            "${superbuild_install_location}/lib"
            "${superbuild_install_location}/Applications/paraview.app/Contents/Libraries")

  if (matplotlib_enabled)
    install(
      DIRECTORY   "${superbuild_install_location}/lib/python2.7/site-packages/matplotlib/mpl-data/"
      DESTINATION "${cmb_package}/${program}.app/Contents/Python/matplotlib/mpl-data"
      COMPONENT   superbuild)
  endif ()

  if (pythonrequests_enabled)
    install(
      FILES       "${superbuild_install_location}/lib/python2.7/site-packages/requests/cacert.pem"
      DESTINATION "${cmb_package}/${program}.app/Contents/Python/requests"
      COMPONENT   superbuild)
  endif ()

  if (paraviewweb_enabled)
    install(
      FILES       "${superbuild_install_location}/Applications/paraview.app/Contents/Python/paraview/web/defaultProxies.json"
      DESTINATION "${cmb_package}/${program}.app/Contents/Python/paraview/web"
      COMPONENT   "superbuild")
    install(
      DIRECTORY   "${superbuild_install_location}/share/paraview/web"
      DESTINATION "${cmb_package}/${program}.app/Contents/Resources"
      COMPONENT   "superbuild")
  endif ()

  foreach (qt5_plugin_path IN LISTS qt5_plugin_paths)
    get_filename_component(qt5_plugin_group "${qt5_plugin_path}" DIRECTORY)
    get_filename_component(qt5_plugin_group "${qt5_plugin_group}" NAME)

    superbuild_apple_install_module(
      "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
      "${program}.app"
      "${qt5_plugin_path}"
      "Contents/Plugins/${qt5_plugin_group}"
      SEARCH_DIRECTORIES  "${library_paths}")
  endforeach ()

  if (cmb_doc_base_dir)
    set(cmb_doc_dir "${cmb_package}/${program}.app/${cmb_doc_base_dir}")
  endif ()

  # Install PDF guides.
  cmb_install_extra_data()
endforeach ()

# FIXME: Install inside of each application?
install(
  DIRECTORY   "${superbuild_install_location}/share/cmb/workflows/"
  DESTINATION "Workflows"
  COMPONENT   superbuild)
