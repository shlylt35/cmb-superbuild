superbuild_add_project(pyarc
  DEPENDS python
  BUILD_IN_SOURCE 1
  CONFIGURE_COMMAND
  "${CMAKE_COMMAND}"
  -E make_directory
  "<INSTALL_DIR>/lib/python2.7/site-packages/PyArc"
  BUILD_COMMAND
  <SOURCE_DIR>/scripts/dependencies.sh
  INSTALL_COMMAND
  tar xf "<SOURCE_DIR>/PyARC.tar.gz"
  -C "<INSTALL_DIR>/lib/python2.7/site-packages/PyArc"
)
