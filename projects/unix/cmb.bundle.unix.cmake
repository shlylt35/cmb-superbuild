set(library_paths
  "${superbuild_install_location}/lib"
  "${superbuild_install_location}/lib/paraview-${paraview_version}"
  "${superbuild_install_location}/lib/cmb-${cmb_version}")

if (Qt5_DIR)
  list(APPEND library_paths
    "${Qt5_DIR}/../..")
endif ()

if (USE_SYSTEM_qt5 AND Qt5_DIR)
  list(APPEND include_regexes
    "libQt5.*")
endif ()

set(plugins)
foreach (executable IN LISTS paraview_executables cmb_programs_to_install)
  if (executable STREQUAL "GrabCuts" OR
    executable STREQUAL "TemplateEditor")
    superbuild_unix_install_program("${superbuild_install_location}/bin/${executable}"
      "lib"
      SEARCH_DIRECTORIES "${library_paths}"
      INCLUDE_REGEXES ${include_regexes}
      EXCLUDE_REGEXES ${exclude_regexes})
  else ()
    superbuild_unix_install_program_fwd("${executable}"
      "lib;lib/paraview-${paraview_version};lib/cmb-${cmb_version}"
      SEARCH_DIRECTORIES "${library_paths}"
      INCLUDE_REGEXES ${include_regexes}
      EXCLUDE_REGEXES ${exclude_regexes})
  endif ()
  list(APPEND plugins
    ${cmb_plugins_${executable}})
endforeach ()

if (plugins)
  list(REMOVE_DUPLICATES plugins)
endif()

foreach (plugin IN LISTS plugins)
  superbuild_unix_install_plugin("lib${plugin}.so"
    "lib"
    "lib;lib/cmb-${cmb_version}"
    SEARCH_DIRECTORIES  "${library_paths}"
    LOCATION            "lib/plugins"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes})
endforeach ()

foreach (plugin IN LISTS cmb_plugins_cmb)
  superbuild_unix_install_plugin("lib${plugin}.so"
    "lib"
    "lib/${plugin}"
    SEARCH_DIRECTORIES  "${library_paths}"
    LOCATION            "lib/plugins"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes}
  )
endforeach ()

foreach (plugin IN LISTS cmb_plugins_standalone)
  superbuild_unix_install_plugin("lib${plugin}.so"
    "lib"
    "lib"
    SEARCH_DIRECTORIES  "${library_paths}"
    LOCATION            "lib/plugins"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes}
  )
endforeach ()

foreach (plugin IN LISTS cmb_plugins_paraview)
  superbuild_unix_install_plugin("lib${plugin}.so"
    "lib"
    "lib/paraview-${paraview_version}/plugins/${plugin}"
    SEARCH_DIRECTORIES  "${library_paths}"
    LOCATION            "lib/paraview-${paraview_version}/plugins/${plugin}/"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes}
  )
endforeach ()

superbuild_unix_install_python(
  LIBDIR              "lib/cmb-${cmb_version}"
  MODULES             ${cmb_python_modules}
  MODULE_DIRECTORIES  "${superbuild_install_location}/lib/python2.7/site-packages"
                      "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
  SEARCH_DIRECTORIES  "${library_paths}"
  INCLUDE_REGEXES ${include_regexes}
  EXCLUDE_REGEXES ${exclude_regexes})

if (cmb_install_paraview_python)
  superbuild_unix_install_python(
    LIBDIR              "lib/paraview-${paraview_version}"
    MODULES             paraview vtkmodules
    MODULE_DIRECTORIES  "${superbuild_install_location}/lib/python2.7/site-packages"
                        "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
    SEARCH_DIRECTORIES  "${library_paths}"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes})

  superbuild_unix_install_python(
    MODULE_DESTINATION  "/site-packages/paraview"
    LIBDIR              "lib/paraview-${paraview_version}"
    MODULES             vtk
    MODULE_DIRECTORIES  "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
    SEARCH_DIRECTORIES  "${library_paths}"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes})
endif ()

if (pythonrequests_enabled)
  install(
    FILES       "${superbuild_install_location}/lib/python2.7/site-packages/requests/cacert.pem"
    DESTINATION "lib/python2.7/site-packages/requests"
    COMPONENT   superbuild)
endif ()

include(python.functions)
superbuild_install_superbuild_python()

if (paraviewweb_enabled)
  install(
    FILES       "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages/paraview/web/defaultProxies.json"
    DESTINATION "lib/python2.7/site-packages/paraview/web"
    COMPONENT   "${paraview_component}")
  install(
    DIRECTORY   "${superbuild_install_location}/share/paraview/web"
    DESTINATION "share/paraview-${paraview_version}"
    COMPONENT   "${paraview_component}")
endif ()

set(plugins_file "${CMAKE_CURRENT_BINARY_DIR}/.plugins")
cmb_add_plugin("${plugins_file}" ${plugins})

install(
  FILES       "${plugins_file}"
  DESTINATION "lib/cmb-${cmb_version}"
  COMPONENT   superbuild)

install(
  DIRECTORY   "${superbuild_install_location}/share/cmb/workflows/"
  DESTINATION "share/cmb/workflows"
  COMPONENT   superbuild)

# ParaView expects this directory to exist.
install(CODE
  "file(MAKE_DIRECTORY \"\$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/lib/paraview-${paraview_version}\")"
  COMPONENT   superbuild)

if (qt5_enabled)
  file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/qt.conf" "")
  install(
    FILES       "${CMAKE_CURRENT_BINARY_DIR}/qt.conf"
    DESTINATION "lib/cmb-${cmb_version}"
    COMPONENT   superbuild)
endif ()

foreach (qt5_plugin_path IN LISTS qt5_plugin_paths)
  get_filename_component(qt5_plugin_group "${qt5_plugin_path}" DIRECTORY)
  get_filename_component(qt5_plugin_group "${qt5_plugin_group}" NAME)

  superbuild_unix_install_plugin("${qt5_plugin_path}"
    "lib"
    "lib/${qt5_plugin_group}/"
    LOADER_PATHS    "${library_paths}"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes})
endforeach ()
