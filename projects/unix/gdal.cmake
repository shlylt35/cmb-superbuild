include("${CMAKE_CURRENT_LIST_DIR}/../gdal.cmake")

superbuild_apply_patch(gdal gcc4-isnan
  "Only define std::isnan when gcc uses update to date libs")
