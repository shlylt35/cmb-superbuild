superbuild_add_project(moab
  DEPENDS eigen hdf5 netcdf
  DEPENDS_OPTIONAL cxx11
  CMAKE_ARGS
    -Wno-dev
    -DBUILD_SHARED_LIBS:BOOL=OFF
    -DCMAKE_CXX_VISIBILITY_PRESET:STRING=hidden
    -DCMAKE_VISIBILITY_INLINES_HIDDEN:BOOL=ON
    -DENABLE_BLASLAPACK:BOOL=OFF
    -DMOAB_HAVE_EIGEN:BOOL=ON
    -DMOAB_USE_SZIP:BOOL=ON
    -DMOAB_USE_CGM:BOOL=OFF
    -DMOAB_USE_CGNS:BOOL=OFF
    -DMOAB_USE_MPI:BOOL=OFF
    -DMOAB_USE_HDF:BOOL=ON
    -DENABLE_HDF5:BOOL=ON # also required to get hdf5 support enabled
    -DMOAB_USE_NETCDF:BOOL=ON
    -DENABLE_NETCDF:BOOL=ON # also required to get ncdf/exodus enabled
    -DNETCDF_ROOT:PATH=<INSTALL_DIR>
    -DMOAB_ENABLE_TESTING:BOOL=ON) # build can't handle this being disabled

superbuild_apply_patch(moab disable-fortran
  "Disable use of fortran")

superbuild_apply_patch(moab export-include-dir
  "Set MOAB and iMesh targets to export their installed include directories")

# By default, linux and os x cmake looks in <INSTALL_DIR>/lib/cmake for
# things. On windows, it does not. So, we set MOAB_DIR to point to the
# location of MOABConfig.cmake for everyone.
superbuild_add_extra_cmake_args(
  -DMOAB_DIR:PATH=<INSTALL_DIR>/lib/cmake/MOAB)
