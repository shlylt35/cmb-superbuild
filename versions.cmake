superbuild_set_revision(boost
  URL     "https://www.paraview.org/files/dependencies/boost_1_66_0.tar.bz2"
  URL_MD5 b2dfbd6c717be4a7bb2d88018eaccf75)

# XXX: When updating this, update the version number in CMakeLists.txt as well.
# The current version of ParaView is version 5.6.0.
set(paraview_revision "6cccdcfdf885a99009a72468fdfa29dbb696984b")

if (USE_PARAVIEW_master)
  set(paraview_revision origin/master)
endif ()
superbuild_set_revision(paraview
  GIT_REPOSITORY "https://gitlab.kitware.com/paraview/paraview.git"
  GIT_TAG        "${paraview_revision}")

superbuild_set_revision(vtkonly
  GIT_REPOSITORY "https://gitlab.kitware.com/vtk/vtk.git"
  GIT_TAG        origin/master)

superbuild_set_selectable_source(cmb
  SELECT 6.0.0
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/cmb.git"
    GIT_TAG         "v6.0.0"
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/cmb.git"
    GIT_TAG         "origin/master"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-cmb")

set(smtk_source_dir)
if (cmb_SOURCE_SELECTION STREQUAL "source")
  set(smtk_source_dir
    "${cmb_SOURCE_DIR}")
else ()
  set(smtk_source_dir
    "${CMAKE_BINARY_DIR}/superbuild/cmb/src")
  set_property(GLOBAL
    PROPERTY
      smtk_need_cmb_download TRUE)
endif ()

superbuild_set_selectable_source(smtk
  SELECT 3.0.0
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "v3.0.0"
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY  "https://gitlab.kitware.com/cmb/smtk.git"
    GIT_TAG         "origin/master"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-smtk")

superbuild_set_revision(cmbworkflows
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/simulation-workflows.git"
  GIT_TAG        origin/master)

superbuild_set_revision(vxl
# VXL master branch @ May 29, 2018
GIT_REPOSITORY "https://github.com/vxl/vxl"
GIT_TAG 4df45773ea8a74d3560b55a05d5fbb2f76741aac)

# Use opencv from Thu Oct 6 13:40:33 2016 +0000
superbuild_set_revision(opencv
  # https://github.com/opencv/opencv.git
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/opencv-dd379ec9fddc1a1886766cf85844a6e18d38c4f1.tar.bz2"
  URL_MD5 19bbd14ed1bd741beccd6d19e444552f)

# Use the tweaked cmake build of zeromq
superbuild_set_revision(zeromq
  # https://github.com/robertmaynard/zeromq4-x.git
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/zeromq4-6d787cf69da6c69550e85a45be1bee1eb0e1c415.tar.bz2"
  URL_MD5 26790786e01a1732a8acf723b99712ba)

# Use remus from Fri Sep 1 15:56:16 2017 +0000
superbuild_set_revision(remus
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/remus.git"
  GIT_TAG        85355887b0da9d8b81bf1de999ba3a0f3ea7eb80)

superbuild_set_revision(gdal
  # https://github.com/judajake/gdal-svn.git
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/gdal-98353693d6f1d607954220b2f8b040375e3d1744.tar.bz2"
  URL_MD5 5aa285dcc856f98ce44020ae1ae192cb)

superbuild_set_revision(eigen
#  # https://bitbucket.org/eigen/eigen/get/034b6c3e1017.zip
#  URL "http://www.computationalmodelbuilder.org/files/dependencies/eigen-eigen-034b6c3e1017.zip"
#  URL_MD5 a2626b3815fa9b0982b4bd8d52583895)
GIT_REPOSITORY "https://github.com/eigenteam/eigen-git-mirror"
GIT_TAG 8b2c28cc4e52c62cbfd457fd94bdf1e91b4f9603)

superbuild_set_revision(moab
  # https://bitbucket.org/fathomteam/moab.git
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/moab-6425a480ffe8e08b96453618fc5530e08e68ae8a.tar.bz2"
  URL_MD5 5946a405cddce972d54044fca6c87b11)

superbuild_set_revision(triangle
  # https://github.com/robertmaynard/triangle.git
  URL     "https://www.paraview.org/files/dependencies/triangle-4c20820448cdfa27f968cfd7cb33ea5b9426ad91.tar.bz2"
  URL_MD5 9a016bc90f1cdff441c75ceb53741b11)

superbuild_set_revision(pythondiskcache
  URL      "https://files.pythonhosted.org/packages/1b/18/ef9b2748bfadc2035d20076869db2dd5d4e22858dd819776bc8f488ec152/diskcache-3.1.0.tar.gz"
  URL_HASH SHA256=96cd1be1240257167a090794cce45db02ecf39d20b7a062580299b42107690ac
)

superbuild_set_revision(pythongirderclient
  URL      "https://files.pythonhosted.org/packages/42/49/a8772bc89b348b0a41e2d583a6aa04d4d9db21a3568a7df17d68029ebb84/girder-client-2.4.0.tar.gz"
  URL_HASH SHA256=1a9c882b8bce2e8233f572a4df9565c678e5614993e6606cdff04251532cddcb
)

superbuild_set_revision(pythonrequests
  URL     "https://pypi.python.org/packages/source/r/requests/requests-2.9.1.tar.gz"
  URL_MD5 0b7f480d19012ec52bab78292efd976d)

superbuild_set_revision(pythonrequeststoolbelt
  URL      "https://files.pythonhosted.org/packages/86/f9/e80fa23edca6c554f1994040064760c12b51daff54b55f9e379e899cd3d4/requests-toolbelt-0.8.0.tar.gz"
  URL_HASH SHA256=f6a531936c6fa4c6cfce1b9c10d5c4f498d16528d2a54a22ca00011205a187b5
)

superbuild_set_revision(pybind11
  # https://github.com/pybind/pybind11.git
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/pybind11-f7bc18f528bb35cd06c93d0a58c17e6eea3fa68c.tar.bz2"
  URL_MD5 275deeab817625e9e9687c42b8a120fa)

superbuild_set_revision(ftgl
  # https://github.com/ulrichard/ftgl.git
  URL     "https://www.paraview.org/files/dependencies/ftgl-dfd7c9f0dee7f0059d5784f3a71118ae5c0afff4.tar.bz2"
  URL_MD5 16e54c7391f449c942f3f12378db238f)

superbuild_set_revision(paraviewwebvisualizer
  URL     "https://www.paraview.org/files/dependencies/visualizer-2.0.12.tar.gz"
  URL_MD5 56e7e241ea6ad66b44469fc3186f47d6)

superbuild_set_revision(paraviewweblightviz
  URL     "https://www.paraview.org/files/dependencies/light-viz-1.16.1.tar.gz"
  URL_MD5 9ac1937cf07ae57bf85c3240f921679a)

superbuild_set_revision(pyarc
  GIT_REPOSITORY "git@kwgitlab.kitware.com:bob.obara/PyARC.git"
  GIT_TAG        reorganization)

superbuild_set_revision(cmbusersguide
  URL     "https://media.readthedocs.org/pdf/cmb/master/cmb.pdf")

superbuild_set_revision(smtkusersguide
  URL     "https://media.readthedocs.org/pdf/smtk/latest/smtk.pdf")

superbuild_set_revision(nlohmannjson
  URL     "http://www.computationalmodelbuilder.org/files/dependencies/json-b406e3704be9a185f1e27e35981ff6d5668dcdc3.tar.bz2"
  URL_MD5 4aa8c3b403f535499cb633650d083150)

superbuild_set_revision(smtkresourcemanagerstate
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/plugins/read-and-write-resource-manager-state.git"
  GIT_TAG        ff5aa02621c0c011d72e4bdc8c4f85c250a88f0f)

superbuild_set_revision(smtkprojectmanager
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/plugins/project-manager.git"
  GIT_TAG        master)
